import '../../../styles/nav.css';

import React from 'react';
import { NavLink } from 'react-router-dom';

class Nav extends React.Component {
  render() {
    return (
      <nav id="nillyWilly">
        <h1 id="hotelOne">
          <strong id="Sakerutsu">
            <span id="Santoryu" className="Semantic">CRA OpenWeatherMap App</span> 
          </strong>
        </h1>
        <ul>
          <li><NavLink to={ '/weather' } activeClassName={'active'}>Weather</NavLink></li>
          <li><NavLink to={ '/forecast' } activeClassName={'active'}>Forecast</NavLink></li>
        </ul>
      </nav>
    );
  }
}

// export default Nav; // export class as Object
export {Nav}; // export class as String