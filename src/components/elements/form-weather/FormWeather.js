import '../../../styles/form-weather.css';

import React from 'react';

// import items as String(s) from exported class as String
import { Weather } from '../../weather/Weather';
import { myLocations } from '../../models/DataModel';
import { OwaApiKey } from '../../models/OwaApiKey'; 

function OptionItem( {location} ){
  let valstring = `${location.id}`;
  return(
    <option value={valstring}> {location.name} </option>
  );
}

class FormWeather extends React.Component {

  constructor(){
    super();    
    this.state = {
      locations: myLocations 
      , selectedvalue: "" 
      , selectedweather: null 
      , cityname: '' 
      , countryname: '' 
      , weatheridcode: '' 
      , currenttemp: '' 
      , hightemp: '' 
      , lowtemp: '' 
      , weathermain: '' 
      , currentdate: '' 
      , windspeed: '' 
      , visibility: '' 
      , humidity: '' 
      , barometricpressure: '' 
      , timesunrise: ''
      , timesunset: '' 
      , weekday: '' 
      , weatherdescription: '' 
    };

    this.onWeatherChange = this.onWeatherChange.bind(this);
    this.onWeatherClick = this.onWeatherClick.bind(this);
  }

  onWeatherClick( event ){
    // console.log( "Click has taken place" );
    // console.log( `this.props: ${ JSON.stringify( this.props ) }` );
    this.fetchCoordinatesWrapper();
    event.preventDefault();
  }

  fetchCoordinatesWrapper(){
    this.locationPromise()
    .then((response)=>{  
      // console.log( response() ); // typeof() returns object
      // return response.json(); // NEITHER A fetch call nor is it necessary
      return response();
    })
    .then( (coordinates) => {
      // console.log( 'coordinates' , coordinates );
      this.runGeoWeather( coordinates );
    } )
    .catch((error)=>{
      console.log(error);
    });
  }

  locationPromise(){ 
    return new Promise( function(resolveHandler,rejectHandler){
      navigator.geolocation.getCurrentPosition( 
        function success(pos) {

          resolveHandler( () => {
            var crdobj = {lat: pos.coords.latitude , lon: pos.coords.longitude };
            // console.log("crdobj: ", crdobj );
            return crdobj;
          } );

          rejectHandler( (err) => { 
            // console.log("derrp" , err );
          } );
        }
        , function error(err) {
          console.warn(`ERROR(${err.code}): ${err.message} , ${err} `); // console.log(err);
        } 
        , {
          enableHighAccuracy: true
          , timeout: 10000
          , maximumAge: 0
        } 
      );
    });
  } // based on https://stackoverflow.com/questions/36995628/how-to-implement-promises-with-the-html5-geolocation-api

  runGeoWeather(coord){
    
    const baseUrl = `https://api.openweathermap.org`;
    const weatherpath = `/data/2.5/weather`;
    const appId = OwaApiKey; 
    const unitTemp = `imperial`; // e.g. -- metric, imperial
    const linguafranca = `en`; // e.g. -- en, es, ja
    const query = `appid=${appId}&units=${unitTemp}&lang=${linguafranca}`;
    
    fetch(`${baseUrl}${weatherpath}?lat=${coord.lat}&lon=${coord.lon}&${query}`)
    .then((response)=>{ 
      // console.log("owapi response" , response);
      return response.json();
    })
    .then((data)=>{

      // console.log("owapi then json data" , data );
      

      this.setState ({
        selectedweather: data
        , cityname: data.name 
        , countryname: data.sys.country 
        , weatheridcode: data.weather[0].id 
        , currenttemp: data.main.temp 
        , hightemp: data.main.temp_max 
        , lowtemp: data.main.temp_min 
        , weathermain: data.weather[0].main 
        , currentdate: data.dt 
        , windspeed: data.wind.speed 
        , visibility: data.visibility 
        , humidity: data.main.humidity 
        , barometricpressure: data.main.pressure 
        , timesunrise: data.sys.sunrise 
        , timesunset: data.sys.sunset 
        , weekday: data.dt 
        , weatherdescription: data.weather[0].description 
      });

    })
    .catch((error)=>{
        console.log(error)
    });
  }

  onWeatherChange( event ){
    this.setState(
      {
        selectedvalue: event.target.value
      }
      , () => {
        setTimeout( this.ifNotEmptyWeather(this.state) , 356 );
      }
    );
  }

  ifNotEmptyWeather( state ){
    if( state.selectedvalue !== ''){
      // console.log( `The location you have chosen is ${ state.selectedvalue }` ); 
      // console.log( `state: ${ JSON.stringify(state) }` ); 
      // console.log( `this.props: ${ JSON.stringify( this.props ) }` );
      this.runChangeWeather( state.selectedvalue );
    }
    else 
    if( state.selectedvalue === '' ){
      // console.log( `Choose a location, please!` );
      this.setState(
        {
          selectedweather: null
          , cityname: '' 
          , countryname: '' 
          , weatheridcode: '' 
          , currenttemp: '' 
          , hightemp: '' 
          , lowtemp: '' 
          , weathermain: '' 
          , currentdate: '' 
          , windspeed: '' 
          , visibility: '' 
          , humidity: '' 
          , barometricpressure: '' 
          , timesunrise: ''
          , timesunset: '' 
          , weekday: '' 
          , weatherdescription: '' 
        }
      );
    }
  }

  runChangeWeather(id){
    
    // console.log( 'id' , id );
    
    const baseUrl = `https://api.openweathermap.org`;
    const weatherpath = `/data/2.5/weather`;
    const appId = OwaApiKey; 
    const unitTemp = `imperial`; // e.g. -- metric, imperial
    const linguafranca = `en`; // e.g. -- en, es, ja
    const query = `appid=${appId}&units=${unitTemp}&lang=${linguafranca}`;
    
    fetch(`${baseUrl}${weatherpath}?id=${id}&${query}`)
    .then((response)=>{ 
      // console.log("owapi response" , response);
      return response.json();
    })
    .then((data)=>{

      // console.log("owapi then json data" , data );
      
      this.setState ({
        selectedweather: data
        , cityname: data.name 
        , countryname: data.sys.country 
        , weatheridcode: data.weather[0].id 
        , currenttemp: data.main.temp 
        , hightemp: data.main.temp_max 
        , lowtemp: data.main.temp_min 
        , weathermain: data.weather[0].main 
        , currentdate: data.dt 
        , windspeed: data.wind.speed 
        , visibility: data.visibility 
        , humidity: data.main.humidity 
        , barometricpressure: data.main.pressure 
        , timesunrise: data.sys.sunrise 
        , timesunset: data.sys.sunset 
        , weekday: data.dt 
        , weatherdescription: data.weather[0].description 
      });

    })
    .catch((error)=>{
        console.log(error)
    });

  }

  render() {
    
    const { locations } = this.state; // console.log( 'locations' , locations );
    // console.log( `this.props: ${ JSON.stringify( this.props ) }` );

    const options = locations.map(
      ( location ) => {
        return(
          <OptionItem key={ location.number.toString() } location={location} />
        );
      }
    );

    return (
      <div id="disburser" className="disbursement">
        <form id="forecaster">
          
          <fieldset className="fold">
            <legend className="lofty">find weather</legend>
            <p className="paraphrase"> Choose any name from the list of more than 70 cities so you can obtain weather information for that location. </p>
            <label className="leftHinge" htmlFor="WhenTheLightsGoDownInThe"><span className="spumoni">city</span></label>
            <div id="developer">
              
              <select id="WhenTheLightsGoDownInThe" onChange={this.onWeatherChange} selectedvalue={this.state.selectedvalue}>
                <option value="">Choose a location</option>
                { options }
              </select>

            </div>
          </fieldset>

          <fieldset className="fold">
            <legend className="lofty">find with global positioning</legend>
            <p className="paraphrase"> Obtain weather information by means of your latitude and longitude. </p>
            <label className="leftHinge" htmlFor="geolo"><span className="spumoni">Geolocation positioning</span></label>
            <div id="positioning">
              
              <button id="geolo" onClick={this.onWeatherClick} >Get Weather</button>

            </div>
          </fieldset>

        </form>
        <section id="sourceOut">
          <Weather state={this.state} />
        </section>
      </div>
    );
  }

}

// export default FormWeather; // export class as Object
export {FormWeather}; // export class as String