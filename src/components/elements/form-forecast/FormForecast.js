import '../../../styles/form-forecast.css';

import React from 'react';

// import items as String(s) from exported class as String
import { Forecast } from '../../forecast/Forecast';
import { myLocations } from '../../models/DataModel';
import { OwaApiKey } from '../../models/OwaApiKey'; 

function OptionItem( {location} ){
  let valstring = `${location.id}`;
  return(
    <option value={valstring}> {location.name} </option>
  );
}

class FormForecast extends React.Component {

  constructor(){
    super();
    this.state = {
      locations: myLocations 
      , selectedvalue: "" 
      , selectedweather: null 
      , firstday: null 
      , remainingfourdays: null  
      , selectedcitylist: null 
      , cityname: '' 
      , countryname: '' 
    };

    this.onForecastChange = this.onForecastChange.bind(this);
    this.onForecastClick = this.onForecastClick.bind(this);
  }

  onForecastClick( event ){
    // console.log( "Click has taken place" );
    // console.log( `this.props: ${ JSON.stringify( this.props ) }` );
    this.fetchCoordinatesWrapper();
    event.preventDefault();
  }

  fetchCoordinatesWrapper(){
    this.locationPromise()
    .then((response)=>{  
      // console.log( response() ); // typeof() returns object
      // return response.json(); // NEITHER A fetch call nor is it necessary
      return response();
    })
    .then( (coordinates) => {
      // console.log( 'coordinates' , coordinates );
      this.runGeoForecast( coordinates );
    } )
    .catch((error)=>{
      console.log(error);
    });
  }

  locationPromise(){ 
    return new Promise( function(resolveHandler,rejectHandler){
      navigator.geolocation.getCurrentPosition( 
        function success(pos) {

          resolveHandler( () => {
            var crdobj = {lat: pos.coords.latitude , lon: pos.coords.longitude };
            // console.log("crdobj: ", crdobj );
            return crdobj;
          } );

          rejectHandler( (err) => {
            console.log("derrp" , err );
          } );
        }
        , function error(err) {
          console.warn(`ERROR(${err.code}): ${err.message} , ${err} `); // console.log(err);
        } 
        , {
          enableHighAccuracy: true
          , timeout: 10000
          , maximumAge: 0
        } 
      );
    });
  } // based on https://stackoverflow.com/questions/36995628/how-to-implement-promises-with-the-html5-geolocation-api

  runGeoForecast(coord){
    
    const baseUrl = `https://api.openweathermap.org`;
    const forecastpath = `/data/2.5/forecast`; 
    const appId = OwaApiKey;
    const unitTemp = `imperial`; // e.g. -- metric, imperial
    const linguafranca = `en`; // e.g. -- en, es, ja
    const query = `appid=${appId}&units=${unitTemp}&lang=${linguafranca}`;
    
    fetch(`${baseUrl}${forecastpath}?lat=${coord.lat}&lon=${coord.lon}&${query}`)
    .then((response)=>{ 
      // console.log("owapi response" , response);
      return response.json();
    })
    .then((data)=>{ 
      
      // console.log("owapi then json data" , data );

      let listoffive = data.list.filter( function(el) {
        return ( el.dt_txt.includes( '12:00:00' ) ); // for normal browsers
        // return ( el.dt_txt.indexOf( '12:00:00' ) !== -1 ); // for benefit of IE11 and less
      } ); 

      const firstday = listoffive.filter(
        function( el, idx, arr ){
          return ( idx === 0 );
        } 
      );

      const remainingfourdays = listoffive.filter(
        function( el, idx, arr ){
          return ( idx !== 0 );
        } 
      );

      this.setState ({
        selectedweather: data 
        , firstday: firstday 
        , remainingfourdays: remainingfourdays 
        , selectedcitylist: listoffive 
        , cityname: data.city.name 
        , countryname: data.city.country 
      });

    })
    .catch((error)=>{
        console.log(error)
    });
  }

  onForecastChange( event ){
    this.setState(
      {
        selectedvalue: event.target.value
      }
      , () => {
        setTimeout( this.ifNotEmptyForecast(this.state) , 356 );
      }
    );
  }

  ifNotEmptyForecast( state ){
    if( state.selectedvalue !== ''){
      // console.log( `The location you have chosen is ${ state.selectedvalue }` ); 
      // console.log( `state: ${ JSON.stringify(state) }` ); 
      // console.log( `this.props: ${ JSON.stringify( this.props ) }` );
      this.runChangeForecast( state.selectedvalue );
    }
    else 
    if( state.selectedvalue === '' ){
      // console.log( `Choose a location, please!` );
      this.setState(
        {
          selectedweather: null 
          , firstday: null 
          , remainingfourdays: null 
          , selectedcitylist: null 
          , cityname: '' 
          , countryname: '' 
        }
      );
    }
  }

  runChangeForecast(id){
    
    // console.log( 'id' , id );
    
    const baseUrl = `https://api.openweathermap.org`;
    const forecastpath = `/data/2.5/forecast`; 
    const appId = OwaApiKey;
    const unitTemp = `imperial`; // e.g. -- metric, imperial
    const linguafranca = `en`; // e.g. -- en, es, ja
    const query = `appid=${appId}&units=${unitTemp}&lang=${linguafranca}`;
    
    fetch(`${baseUrl}${forecastpath}?id=${id}&${query}`)
    .then((response)=>{ 
      // console.log("owapi response" , response);
      return response.json();
    })
    .then((data)=>{ 
      
      // console.log("owapi then json data" , data );

      let listoffive = data.list.filter( function(el) {
        return ( el.dt_txt.includes( '12:00:00' ) ); // for normal browsers
        // return ( el.dt_txt.indexOf( '12:00:00' ) !== -1 ); // for benefit of IE11 and less
      } ); 

      const firstday = listoffive.filter(
        function( el, idx, arr ){
          return ( idx === 0 );
        } 
      );

      const remainingfourdays = listoffive.filter(
        function( el, idx, arr ){
          return ( idx !== 0 );
        } 
      );

      this.setState ({
        selectedweather: data 
        , firstday: firstday 
        , remainingfourdays: remainingfourdays 
        , selectedcitylist: listoffive 
        , cityname: data.city.name 
        , countryname: data.city.country 
      });

    })
    .catch((error)=>{
        console.log(error)
    });

  }

  render() {

    const { locations } = this.state; // console.log( 'locations' , locations );
    // console.log( `this.props: ${ JSON.stringify( this.props ) }` );

    const options = locations.map(
      ( location ) => {
        return(
          <OptionItem key={ location.number.toString() } location={location} />
        );
      }
    );

    return (
      <div id="disburser" className="disbursement">
        <form id="forecaster">

          <fieldset className="fold">
            <legend className="lofty">find a forecast</legend>
            <p className="paraphrase"> Choose any name from the list of more than 70 cities so you can obtain weather information for that location. </p>
            <label className="leftHinge" htmlFor="WhenTheLightsGoDownInThe"><span className="spumoni">city</span></label>
            <div id="developer">
              
            <select id="WhenTheLightsGoDownInThe" onChange={this.onForecastChange} selectedvalue={this.state.selectedvalue}>
                <option value="">Choose a location</option>
                { options }
              </select>

            </div>
          </fieldset>

          <fieldset className="fold">
            <legend className="lofty">find with global positioning</legend>
            <p className="paraphrase"> Obtain weather information by means of your latitude and longitude. </p>
            <label className="leftHinge" htmlFor="geolo"><span className="spumoni">Geolocation positioning</span></label>
            <div id="positioning">
              
              <button id="geolo" onClick={this.onForecastClick} >Get Forecast</button>

            </div>
          </fieldset>

        </form>
        <section id="sourceOut">
          <Forecast state={this.state} />
        </section>
      </div>
    );
  }

}

// export default FormForecast; // export class as Object
export {FormForecast}; // export class as String
