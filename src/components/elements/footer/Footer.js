import '../../../styles/footer.css';

import React from 'react';

class Footer extends React.Component {
  
  render() {

    let newDate = new Date();
    let myPresentYear = newDate.getFullYear();

    return (
      <footer id="farewell">
        <p id="profess">Concept, artwork, etc. <wbr />by  &lsquo;don Lucho&rsquo; <wbr />(yada, yada, yada) <wbr />&copy; <wbr /> { myPresentYear } </p>
      </footer>
    );
  }
}

// export default Footer; // export class as Object
export {Footer}; // export class as String