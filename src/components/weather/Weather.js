import imageUrl from '../../assets/layout/new_owa_371x41.gif';
import '../../styles/weather.css';

import React from 'react';

const Weather = ( {state} ) => {

  // const mystate = state; console.log( `mystate: ${ JSON.stringify(mystate) }` ); 
  // const selectedvalue = state.selectedvalue; console.log( `selectedvalue: ${ selectedvalue }` ); 
  
  function noOpLink( event ){
    event.preventDefault(); 
  }

  function singleDayStringer( unixtimestamp ){ 
    let nowObj = new Date();
    let tzoffset = -( nowObj.getTimezoneOffset() ); 
    let timezoneoffset_milliseconds = tzoffset * 60 * 1000 ; 
    let dateObj = new Date( (unixtimestamp * 1000) + timezoneoffset_milliseconds ); 
    let arr_days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']; 
    let day = arr_days[dateObj.getUTCDay()]; 
    let timestring = `${day}`;
    return timestring;
  }

  function dateStringer( unixtimestamp ){ 
    let nowObj = new Date();
    // getTimezoneOffset() returns the time difference between UTC time and local time in minutes.
    let tzoffset = -( nowObj.getTimezoneOffset() ); // -360 (in minutes) eq. to -6 hours from GMT
    // this returns getTimezoneOffset in seconds (* 60 ), then, in milliseconds (* 1000 )
    let timezoneoffset_milliseconds = tzoffset * 60 * 1000 ; // -21600000 milliseconds
    let dateObj = new Date( (unixtimestamp * 1000) + timezoneoffset_milliseconds ); 
    let arr_months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    let month = arr_months[dateObj.getUTCMonth()]; 
    let date = dateObj.getUTCDate(); 
    let year = dateObj.getUTCFullYear(); 
    let hours = dateObj.getUTCHours(); 
    let minutes = dateObj.getUTCMinutes(); 
    let seconds = dateObj.getUTCSeconds(); 
    let amPm = hours >= 12 ? 'pm' : 'am'; 
    let hourstostring = ( "0" + ( ( hours % 12 ) || 12 ) ).substr(-2);
    let minutestostring = ("0" + minutes).substr(-2);
    let secondstostring = ("0" + seconds).substr(-2);    
    let timestring = `${month} ${date}, ${year} at ${hourstostring}:${minutestostring}:${secondstostring} ${amPm}`;
    return timestring;
  }

  function sunriseStringer( unixtimestamp ){
    let nowObj = new Date();
    // getTimezoneOffset() returns the time difference between UTC time and local time in minutes.
    let tzoffset = -( nowObj.getTimezoneOffset() ); // -360 (in minutes) eq. to -6 hours from GMT
    // this returns getTimezoneOffset in seconds (* 60 ), then, in milliseconds (* 1000 )
    let timezoneoffset_milliseconds = tzoffset * 60 * 1000 ; // -21600000 milliseconds
    let dateObj = new Date( (unixtimestamp * 1000) + timezoneoffset_milliseconds ); 
    let hours = dateObj.getUTCHours(); 
    let minutes = dateObj.getUTCMinutes(); 
    let seconds = dateObj.getUTCSeconds(); 
    let amPm = hours >= 12 ? 'pm' : 'am'; 
    let hourstostring = ( "0" + ( ( hours % 12 ) || 12 ) ).substr(-2);
    let minutestostring = ("0" + minutes).substr(-2);
    let secondstostring = ("0" + seconds).substr(-2);    
    let timestring = `${hourstostring}:${minutestostring}:${secondstostring} ${amPm}`;
    return timestring;
  }

  function sunsetStringer( unixtimestamp ){
    let nowObj = new Date();
    // getTimezoneOffset() returns the time difference between UTC time and local time in minutes.
    let tzoffset = -( nowObj.getTimezoneOffset() ); // -360 (in minutes) eq. to -6 hours from GMT
    // this returns getTimezoneOffset in seconds (* 60 ), then, in milliseconds (* 1000 )
    let timezoneoffset_milliseconds = tzoffset * 60 * 1000 ; // -21600000 milliseconds
    let dateObj = new Date( (unixtimestamp * 1000) + timezoneoffset_milliseconds ); 
    let hours = dateObj.getUTCHours(); 
    let minutes = dateObj.getUTCMinutes(); 
    let seconds = dateObj.getUTCSeconds(); 
    let amPm = hours >= 12 ? 'pm' : 'am'; 
    let hourstostring = ( "0" + ( ( hours % 12 ) || 12 ) ).substr(-2);
    let minutestostring = ("0" + minutes).substr(-2);
    let secondstostring = ("0" + seconds).substr(-2);    
    let timestring = `${hourstostring}:${minutestostring}:${secondstostring} ${amPm}`;
    return timestring;
  }

  const selectedweather = state.selectedweather; //console.log( `selectedweather: ${ JSON.stringify( selectedweather ) }` ); 
  const cityname = state.cityname;
  const countryname = state.countryname;
  const weatheridcode = `/images/weathercodes/${ state.weatheridcode }.gif`; 
  const currenttemp = Math.round( state.currenttemp );
  const hightemp = Math.round( state.hightemp );
  const lowtemp = Math.round( state.lowtemp );
  const weathermain = state.weathermain;
  const currentdate = dateStringer( state.currentdate );
  const windspeed = Math.round( state.windspeed );
  const visibility = state.visibility;
  const humidity = state.humidity;
  const barometricpressure = state.barometricpressure;
  const timesunrise = sunriseStringer( state.timesunrise );
  const timesunset = sunsetStringer( state.timesunset );
  const weekday = singleDayStringer( state.weekday );
  const weatherdescription = `${ state.weatherdescription.charAt(0).toUpperCase() }${state.weatherdescription.substr(1)}`;

  if( !selectedweather ){
    return(
      <p id="pFrag">
        <img id="initialIcon" className="strongImage" alt="" src={ imageUrl } />
      </p>
    );
  }

  return (
    <div id="divFrag">

      <div className="hgroup" id="home">
        <a className="strongAnchor dipOutElsewhere" title="Link to some place" href="" onClick={ noOpLink } target="_blank">
          <img id="strongImage" className="strongImage" alt="" src={ imageUrl } />
        </a>
        <h2 id="hotelTwo"> { cityname } Weather Forecasts</h2>
        <h3 id="hotelThree">Nearest weather station: { cityname }, { countryname } &nbsp;</h3>
      </div>
      
      <div id="dayZero">
        <h4 id="hotelFour">Current Weather in { cityname }</h4>
        <figure id="digThisFig" className="checkIt allClear">
          <img src={ weatheridcode } alt="" className="dejaVu"/>
          <figcaption className="esFatale allClear">
            <span id="degreeCurrentTemp"> { currenttemp }&deg; F </span>
            <span id="highTemp">High: { hightemp }&deg; F </span>
            <span id="lowTemp">Low: { lowtemp }&deg; F </span>
            <span id="shortDescriptor"> { weathermain } </span>
          </figcaption>
        </figure>
        <div id="subSection">
          <em id="muchtoomuchInfo">Current conditions as of { currentdate }</em>
          <dl id="datum">
            <dt id="dtOne" className="day">Wind:</dt>
            <dd id="ddOne" className="detail">Winds at <span className="spanInMiles"> { windspeed } <abbr className="anotherClazz">mph </abbr></span>.</dd>
            <dt id="dtTwo" className="day">Visibility: </dt>
            <dd id="ddTwo" className="detail"> { visibility } meters </dd>
            <dt id="dtThree" className="day">Humidity: </dt>
            <dd id="ddThree" className="detail">{ humidity } &#37;</dd>
            <dt id="dtFour" className="day">Barometric Pressure: </dt>
            <dd id="ddFour" className="detail"> { barometricpressure } hPa</dd>
            <dt id="dtFive" className="day">Sunrise (local time):</dt>
            <dd id="ddFive" className="detail"><span className="spotCap"> { timesunrise } </span></dd>
            <dt id="dtSix" className="day">Sunset (local time):</dt>
            <dd id="ddSix" className="detail"><span className="spotCap"> { timesunset } </span></dd>
          </dl>
        </div>
      </div>

      <div id="DLuffy">
        <div className="fourByTwos" id="detailedLocalForecast">
          <ul id="updatedJive">
          </ul>
        </div>
      </div>

      <div id="details" className="details">
        <h5 id="hotelFive">Detailed Local Forecast for { cityname }</h5>
        <ul id="umbrella">
          <li className="lightning">
            <strong className="specialty">{ weekday }. </strong>&nbsp;
            { weatherdescription }.&nbsp;
            High { hightemp } &deg; F.&nbsp;
            Low { lowtemp } &deg; F.&nbsp;
            Winds up to { windspeed } mph in some cases.
          </li>
        </ul>
      </div>

    </div>
  );
};

// export default Weather; // export class as Object
export {Weather}; // export class as String