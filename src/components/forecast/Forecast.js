import imageUrl from '../../assets/layout/new_owa_371x41.gif';
import '../../styles/forecast.css';

import React from 'react';

class Forecast extends React.Component { 

  constructor(props){
    super(props); // console.log( 'props' , props );
    this.noOpLink = this.noOpLink.bind(this);
  }

  noOpLink( event ){
    event.preventDefault(); 
  }

  singleDayStringer( unixtimestamp ){ 
    let dateObj = new Date( (unixtimestamp * 1000) ); 
    let arr_days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']; 
    let day = arr_days[dateObj.getDay()]; 
    let timestring = `${day}`;
    return timestring;
  }

  render() {

    // console.log( 'this.props' , this.props );
    // const mystate = this.props.state; // console.log( `mystate: ${ JSON.stringify(mystate) }` ); 
    // const selectedvalue = this.props.state.selectedvalue; // console.log( `selectedvalue: ${ selectedvalue }` ); 

    const selectedweather = this.props.state.selectedweather; // console.log( `selectedweather: ${ JSON.stringify( selectedweather ) }` ); 
    const cityname = this.props.state.cityname;
    const countryname = this.props.state.countryname; 
    const firstday = this.props.state.firstday; // console.log( 'firstday' , firstday );
    const remainingfourdays = this.props.state.remainingfourdays; // console.log( 'remainingfourdays' , remainingfourdays ); 
    const selectedcitylist = this.props.state.selectedcitylist; // console.log( 'selectedcitylist' , selectedcitylist ); 
    
    if( !selectedweather ){
      return(
        <p id="pFrag">
          <img id="initialIcon" className="strongImage" alt="" src={ imageUrl } />
        </p>
      );
    }

    return (
      <div id="divFrag">
        
        <div className="hgroup" id="home">
          <a className="strongAnchor dipOutElsewhere" title="Link to some place" href="" onClick={ this.noOpLink } target="_blank">
            <img id="strongImage" className="strongImage" alt="" src={ imageUrl } />
          </a>
          <h2 id="hotelTwo"> { cityname } Weather Forecasts</h2>
          <h3 id="hotelThree">Nearest weather station: { cityname }, { countryname } &nbsp;</h3>
        </div>

        <div id="dayZero">
          <h4 id="hotelFour">Current Weather in { cityname }</h4>
          { firstday.map( 
            (day) => <figure key={day.dt} id="digThisFig" className="checkIt allClear">
              <img src={ `/images/weathercodes/${ day.weather[0].id }.gif` } alt="" className="dejaVu"/>
              <figcaption className="esFatale allClear" >
                <span id="degreeCurrentTemp" > { Math.round( day.main.temp ) }&deg; F </span>
                <span id="highTemp" >High: { Math.round( day.main.temp_max ) }&deg; F </span>
                <span id="lowTemp" >Low: { Math.round( day.main.temp_min ) }&deg; F </span>
                <span id="shortDescriptor" > { day.weather[0].main } </span>
              </figcaption>
            </figure>
          ) }
          { firstday.map( 
            (day) => <div key={day.dt} id="subSection">
              <em id="muchtoomuchInfo" >Forecasted weather for { day.dt_txt }</em>
              <dl id="datum" >
                <dt id="dtOne" className="day">Wind:</dt>
                <dd id="ddOne" className="detail">Winds at <span className="spanInMiles"> { Math.round( day.wind.speed ) } <abbr className="anotherClazz">mph </abbr></span>.</dd>
                <dt id="dtThree" className="day">Humidity: </dt>
                <dd id="ddThree" className="detail">{ day.main.humidity } &#37;</dd>
                <dt id="dtFour" className="day">Barometric Pressure: </dt>
                <dd id="ddFour" className="detail"> { Math.round( day.main.pressure ) } hPa</dd>
              </dl>
            </div>
          ) }
        </div>

        <div id="DLuffy">
          <div className="fourByTwos" id="detailedLocalForecast">
            <ul id="updatedJive">
              { remainingfourdays.map( 
                (day) => <li key={day.dt} className="liquidLows">
                  <strong className="specialty">{ this.singleDayStringer( day.dt ) }</strong>
                  <span className="stressBold"> { day.weather[0].description } </span>
                  <img className="illustrative" src={ `/images/weathercodes/${ day.weather[0].id }.gif` } alt="" />
                  <span className="skyrocket">High: { Math.round( day.main.temp_max ) } &deg; F. </span>
                  <span className="showLow">Low: { Math.round( day.main.temp_min ) } &deg; F.</span>
                </li>
              ) }
            </ul>
          </div>
        </div>

        <div id="details" className="details">
          <h5 id="hotelFive">Detailed Local Forecast for { cityname }</h5>
          <ul id="umbrella">
            { selectedcitylist.map( 
              (day) => <li key={day.dt} className="lightning">
                <strong className="specialty">{ this.singleDayStringer( day.dt ) }</strong>&nbsp;
                { `${ day.weather[0].description.charAt(0).toUpperCase() }${day.weather[0].description.substr(1)}` }.
                High { Math.round( day.main.temp_max ) } &deg; F.
                Low { Math.round( day.main.temp_min ) } &deg; F.
                Winds up to { Math.round( day.wind.speed ) } mph in some cases. 
              </li>
            ) }
          </ul>
        </div>

      </div>
    );
  }
}

// export default Forecast; // export class as Object
export {Forecast}; // export class as String