import './styles/App.css';

import React from 'react';

// import items as Object(s) from class exported as Object
// import Nav from './components/elements/nav/Nav'; 
// import Footer from './components/elements/footer/Footer';

// import items as String(s) from exported class as String
import {Nav} from './components/elements/nav/Nav'; 
import {Footer} from './components/elements/footer/Footer';

class App extends React.Component { 

  // constructor(){
  //   super();
  // }

  render() {
    return (
      <article>

        <Nav />
        
        <React.Fragment>
          { this.props.children }
        </React.Fragment>
        
        <Footer />
      </article>
    );
  }
}

// export default App; // export class as Object
export {App}; // export class as String