import './styles/index.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter , Switch, Route,  Redirect } from 'react-router-dom';

// import items as Object(s) from class exported as Object
// import App from './App';  
// import FormWeather from './components/elements/form-weather/FormWeather';
// import FormForecast from './components/elements/form-forecast/FormForecast';

// import items as String(s) from exported class as String
import {App} from './App'; 
import {FormWeather} from './components/elements/form-weather/FormWeather';
import {FormForecast} from './components/elements/form-forecast/FormForecast';

import registerServiceWorker from './registerServiceWorker';

class Index extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <App>
          <Switch>
            <Route exact={true} path={ '/weather' } component={ FormWeather } ></Route>
            <Route exact={true} path={ '/forecast' } component={ FormForecast } ></Route>
            <Redirect to="/weather" />
          </Switch>
        </App>
      </BrowserRouter>
    );
  }
}

ReactDOM.render(<Index />, document.getElementById('root'));
registerServiceWorker();
