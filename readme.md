# Description
Fun with Create React App and the OpenWeatherMap API.

## What it has
It makes limited use of the Geolocation API by utilizing the getCurrentPosition method. You will find that there are dedicated components for each of the **forecast** and **weather** calls to the OpenWeatherMap API. Both of the responses are different (no surprise here) and both are available thanks to the use of the **React Router DOM BrowserRouter module**.

## Please visit
You can see [the lesson at surge.sh](https://openweather-app-cra.surge.sh "the lesson at surge") and decide if this is something for you to play around with at a later date.

## This is the companion project that was in the making.
You can see the [original repository](https://gitlab.com/artavia/openweather-app-ng5 "the lesson at gitlab"), then, decide if that is something for you to play around with, too.

## Or, just visit Create React App v3 version of the project today
You can see [the lesson at surge.sh](https://cra3-owa-bulma-app.surge.sh "the lesson at surge") and decide if this is something for you to play around with at a later date.

## I am back
I am finally back working on React again after a two (2) month hiatus. I was concentrating by and far on working with Angular5 during that time. On the other hand, I slapped this project together in about three (3) days. And, I will admit that I spent the first half day staring at the screen out of pure shock! I felt a little lost after being neck&#45;deep in Angular for a couple of months. I guess this is a symptom of being conscious of my own mortality! But once things clicked, things became super simple and I recalled that working with ReactJS is simpler in a lot of ways than working with Angular5!

## For the anticipated naysayers and the critics from the peanut gallery
First, I want to thank God&#45; the living God of the Holy Bible. Second and third, I would also like to thank Christ Jesus and the Second Comforter for giving me the necessary endurance and patience required to complete this project. I am small but without a doubt I get my strength from the Holy Trinity. Nothing or nobody inspires me. I am motivated by my faith in God only. Finally, and while looking directly at the adversary specifically, let me rhetorically pose this question to you: **If you think that my project is so bad or tacky, then, where is your project or original work so we can compare?** And, good luck with those devil horns, too. (Yeah, right!)

## My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!